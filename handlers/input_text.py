from textwrap import dedent
from utils.logs import Logger
logger = Logger().get_logger()


class InputTextHandler:
    def __init__(self, func):
        self.func = func

    def __call__(self, event, context, **kwargs):
        logger.debug(f"Event: {event}")

        html = """
        <div className={wrapperClass}>
            <label htmlFor={this.props.name}>{this.props.label}</label>
            <div className='field'>
                <input type='text'
                    name={this.props.name}
                    className={this.props.inputClass}
                    placeholder={this.props.placeholder}
                    ref={this.props.name}
                    onChange={this.props.onChange}
                    value={this.props.value} 
                />
                <div className='input'>{this.props.error}</div>
            </div>
        </div> """

        html = html.replace('\n', '<br>')
        # Update speechlet dict
        kwargs.update({
            "body": {"html": dedent(html)},
        })

        return self.func(event, context, **kwargs)
