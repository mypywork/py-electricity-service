from intents.get_intent import GetIntent
from intents.create_intent import CreateIntent
from intents.filter_intent import FilterIntent
from intents.help_intent import HelpIntent
from intents.stop_intent import StopIntent
from intents.cancel_intent import CancelIntent
from services.speechlets import AlexaSpeechletService
from intents.launch_request import LaunchRequest
from intents.end_request import SessionEndedRequest
from utils.logs import Logger
logger = Logger().get_logger()


class ElectricityHandler:
    def __init__(self, func):
        self.func = func

    def __call__(self, event, context, **kwargs):
        request_type = event['request']['type']
        logger.debug(f"Request Type: {request_type}")

        # Intialize Params
        should_end_session = True
        card_title = 'Electricity'
        card_content = 'Good Bye'
        speech_text = 'Good Bye'
        session_attributes = {}

        if request_type == 'LaunchRequest':
            lreuqest = LaunchRequest()
            should_end_session = lreuqest.should_end_session
            speech_text = lreuqest.speech_text
            card_content = lreuqest.card_content
        elif request_type == 'IntentRequest':
            intent = event['request']['intent']['name']
            intent = intent.split('AMAZON.')[1] if 'AMAZON.' in intent else intent

            intent = eval(intent)(event=event)
            should_end_session = intent.should_end_session
            speech_text = intent.speech_text
            card_content = intent.card_content
        elif request_type == 'SessionEndedRequest':
            erequest = SessionEndedRequest()
            should_end_session = erequest.should_end_session
            speech_text = erequest.speech_text
            card_content = erequest.card_content

        # Speechlet params
        speechlet_params = {
            "session_attributes": session_attributes,
            "speech_text": speech_text,
            "card_title": card_title,
            "card_content": card_content
        }

        # Generate Speechlet content
        speechlet = AlexaSpeechletService(**speechlet_params)
        speech, card, reprompt, session = speechlet.all()

        # Update speechlet dict
        kwargs.update({
            "should_end_session": should_end_session,
            "build_speech": speech,
            "build_card": card,
            "build_reprompt": reprompt,
            "build_session_attributes": session
        })

        return self.func(event, context, **kwargs)
