class AlexaSpeechletService:
    version = "1.0"

    def __init__(self, session_attributes=dict, speech_type="PlainText", speech_text=None, speech_ssml=None, card_type="Simple", card_title=None, card_content=None, card_text=None):
        self.speech_type = speech_type
        self.speech_text = speech_text
        self.speech_ssml = speech_ssml
        self.card_type = card_type
        self.card_title = card_title
        self.card_content = card_content
        self.card_text = card_text
        self.session_attributes = session_attributes

    def build_speech(self):
        speech = {
            "type": self.speech_type,
            "text": self.speech_text
        }
        return speech

    def build_card(self):
        card = {
            "type": self.card_type,
            "title": self.card_title,
            "content": self.card_content
        }
        return card

    def build_reprompt(self):
        reprompt = {
            "outputSpeech": {
                "type": self.speech_type,
                "text": self.speech_text
            }
        }
        return reprompt

    def build_session_attributes(self):
        return self.session_attributes

    def all(self):
        return self.build_speech(), self.build_card(), self.build_reprompt(), self.build_session_attributes()
