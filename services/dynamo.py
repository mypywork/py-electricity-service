import os
from utils.logs import Logger
logger = Logger().get_logger()


class DynamoQueryManager(object):
    def __init__(self, connection):
        self.connection = connection
        self.table = ".".join([os.environ.get('STAGE', 'DEVELOP').upper(), os.environ.get('TABLE', 'ELECTRICITY')])

    def create(self, userid, keys, values):
        params = {
            "TableName": self.table,
            "UpdateExpression": 'SET ' + ', '.join([f"#key_{key} = :key_{key}" for key in keys]),
            "ExpressionAttributeNames": {f"#key_{key}": key for key in keys},
            "ExpressionAttributeValues": dict(zip(map(lambda key: f":key_{key}", keys), list(map(lambda value: {'S': value}, values)))),
            "Key": {'id': {'S': userid}}
        }
        logger.debug(f"Dynamo params: {params}")
        resp = self.connection.update_item(**params)
        logger.debug(f"Dynamo Response: {resp}")
        return resp

    def read(self, userid, unixdate):
        params = {
            'TableName': self.table,
            'ProjectionExpression': f"#key_{unixdate}",
            'ExpressionAttributeNames': {f"#key_{unixdate}": unixdate},
            'ExpressionAttributeValues': {':key_id': {'S': userid}},
            'KeyConditionExpression': 'id = :key_id'
        }
        resp = self.connection.query(**params)
        logger.debug(f"Dynamo Response: {resp}")
        return resp
