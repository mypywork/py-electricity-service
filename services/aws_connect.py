import boto3


class AwsResourceConnection:
    def __init__(self, resource=None, region=None, access_key=None, secret_key=None):
        self.resource = resource
        self.region = region
        self.access_key = access_key
        self.secret_key = secret_key

    def connect(self):
        if self.access_key is None:
            connection = boto3.client(self.resource, region_name=self.region)
        else:
            connection = boto3.client(self.resource, region_name=self.region,
                                      aws_access_key_id=self.access_key,
                                      aws_secret_access_key=self.secret_key)
        return connection

