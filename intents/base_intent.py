class BaseIntent:
    def __init__(self, *args, **kwargs):
        self.should_end_session = True
        self._speech_text = None
        self._card_content = None

    @property
    def speech_text(self):
        return ''

    @speech_text.setter
    def speech_text(self, value):
        self._speech_text = value

    @property
    def card_content(self):
        return ''

    @card_content.setter
    def card_content(self, value):
        self._card_content = value
