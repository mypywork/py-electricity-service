from .base_intent import BaseIntent
from utils.utility import DateTimeUtility
from services.aws_connect import AwsResourceConnection
from services.dynamo import DynamoQueryManager
from utils.logs import Logger
logger = Logger().get_logger()


class FilterIntent(BaseIntent):
    def __init__(self, event=None, **kwargs):
        super().__init__(**kwargs)
        self.event = event
        self.should_end_session = False
        self.startdate = event.get('request').get('intent').get('slots', {}).get('datestart', {}).get('value')
        self.enddate = event.get('request').get('intent').get('slots', {}).get('dateend', {}).get('value')
        self.duration = event.get('request').get('intent').get('slots', {}).get('duration', {}).get('value')
        self.reading = self._reading()

    @property
    def speech_text(self):
        msg = f"Total reading between {self.startdate} and {self.enddate} is {self.reading}" if self.reading is not None else "Please mention a valid date"
        return msg

    @property
    def card_content(self):
        msg = f"Total reading between {self.startdate} and {self.enddate} is {self.reading}" if self.reading is not None else "Please mention a valid date"
        return msg

    def _reading(self):
        connection = AwsResourceConnection('dynamodb', 'us-east-1').connect()
        manager = DynamoQueryManager(connection)
        if self.startdate and self.enddate:
            startunixdate = DateTimeUtility.unixdate_fromstring(self.startdate)
            endunixdate = DateTimeUtility.unixdate_fromstring(self.enddate)

            params = {"userid": self.event.get("session").get("user").get("userId"), "unixdate": startunixdate}
            resp = manager.read(**params)
            startreading = resp.get('Items')[0].get(startunixdate, {}).get('S', 0) if resp.get('Items') else '0'

            params = {"userid": self.event.get("session").get("user").get("userId"), "unixdate": endunixdate}
            resp = manager.read(**params)
            endreading = resp.get('Items')[0].get(endunixdate, {}).get('S', 0) if resp.get('Items') else '0'

            reading = int(endreading) - int(startreading)
            return reading
        elif self.duration:
            return None
        else:
            return None



