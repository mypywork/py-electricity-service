from .base_intent import BaseIntent
from utils.utility import DateTimeUtility
from services.aws_connect import AwsResourceConnection
from services.dynamo import DynamoQueryManager
from utils.logs import Logger
logger = Logger().get_logger()


class CreateIntent(BaseIntent):
    def __init__(self, event=None):
        super().__init__()
        self.should_end_session = False
        self.event = event
        self.stringdate = event.get('request').get('intent').get('slots', {}).get('date', {}).get('value')
        self.reading = event.get('request').get('intent').get('slots', {}).get('reading', {}).get('value', "0")
        self.resp = self._resp()

    @property
    def speech_text(self):
        msg = f"I have added {self.reading} for {self.stringdate}" if self.resp else "Please mention a valid date"
        return msg

    @property
    def card_content(self):
        msg = f"I have added {self.reading} for {self.stringdate}" if self.resp else "Please mention a valid date"
        return msg

    def _resp(self):
        if self.stringdate:
            unixdate = DateTimeUtility.unixdate_fromstring(self.stringdate)
            params = {"userid": self.event.get("session").get("user").get("userId"), "keys": [unixdate], "values": [self.reading]}
            connection = AwsResourceConnection('dynamodb', 'us-east-1').connect()
            manager = DynamoQueryManager(connection)
            resp = manager.create(**params)
            return resp
        else:
            return None


