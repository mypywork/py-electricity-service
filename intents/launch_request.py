from .base_intent import BaseIntent


class LaunchRequest(BaseIntent):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.should_end_session = False

    @property
    def speech_text(self):
        return f"Welcome Shashank to Electricity Management"

    @property
    def card_content(self):
        return f"Welcome Shashank to Electricity Management"
