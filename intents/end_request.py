from .base_intent import BaseIntent


class SessionEndedRequest(BaseIntent):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.should_end_session = True

    @property
    def speech_text(self):
        return "Good Bye"

    @property
    def card_content(self):
        return "Good Bye"
