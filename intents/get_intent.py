from .base_intent import BaseIntent
from utils.utility import DateTimeUtility
from services.aws_connect import AwsResourceConnection
from services.dynamo import DynamoQueryManager
from utils.logs import Logger
logger = Logger().get_logger()


class GetIntent(BaseIntent):
    def __init__(self, event=None, **kwargs):
        super().__init__(**kwargs)
        self.should_end_session = False
        self.event = event
        self.stringdate = event.get('request').get('intent').get('slots', {}).get('date', {}).get('value')
        self.reading = self._reading()

    @property
    def speech_text(self):
        msg = f"Reading for {self.stringdate} is {self.reading}" if self.reading is not None else "Please mention a valid date"
        return msg

    @property
    def card_content(self):
        msg = f"Reading for {self.stringdate} is {self.reading}" if self.reading is not None else "Please mention a valid date"
        return msg

    def _reading(self):
        if self.stringdate:
            unixdate = DateTimeUtility.unixdate_fromstring(self.stringdate)
            params = {"userid": self.event.get("session").get("user").get("userId"), "unixdate": unixdate}
            connection = AwsResourceConnection('dynamodb', 'us-east-1').connect()
            manager = DynamoQueryManager(connection)
            resp = manager.read(**params)
            reading = resp.get('Items')[0].get(unixdate, {}).get('S', 0) if resp.get('Items') else '0'
            return reading
        else:
            return None
