from .base_intent import BaseIntent


class CancelIntent(BaseIntent):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.should_end_session = True

    @property
    def speech_text(self):
        return "Ok, I'm cancelling the request"

    @property
    def card_content(self):
        return "Ok, I'm cancelling the request"
