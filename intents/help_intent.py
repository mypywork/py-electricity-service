from .base_intent import BaseIntent


class HelpIntent(BaseIntent):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.should_end_session = False

    @property
    def speech_text(self):
        return "I'm happy to help you"

    @property
    def card_content(self):
        return "I'm happy to help you"
