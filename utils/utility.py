from datetime import datetime, timedelta
import time


class Singleton(type):
    instance = None

    def __call__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = super().__call__(*args, **kwargs)
        return cls.instance


class Cors:
    def __init__(self, enable=False):
        self.enable = enable

    def __call__(self, func, *args, **kwargs):
        def wrapper(*args, **kwargs):
            if self.enable:
                kwargs['headers'] = {"Content-Type": "application/json",
                                     "Access-Control-Allow-Origin": "*",
                                     "Access-Control-Allow-Credentials": True
                                     }
            else:
                kwargs['headers'] = {"Content-Type": "application/json"}
            return func(*args, **kwargs)
        return wrapper


class DateTimeUtility:
    @staticmethod
    def unixdate_fromstring(date):
        if date:
            unix_date = str(int(time.mktime(datetime.strptime(date, "%Y-%m-%d").date().timetuple())))
        else:
            unix_date = str(int(time.mktime(datetime.today().date().timetuple())))
        return unix_date

    @staticmethod
    def unixdate_fromobject(date):
        unix_date = str(int(time.mktime(date.date().timetuple())))
        return unix_date

    @staticmethod
    def stringdate_fromunix(unixtimestamp):
        return datetime.fromtimestamp(int(unixtimestamp)).strftime("%d %B, %Y")

    @staticmethod
    def unixdaterange_fromstrings(startdate, enddate):
        startdate = datetime.strptime(startdate, "%Y-%m-%d")
        enddate = datetime.strptime(enddate, "%Y-%m-%d")
        delta = enddate - startdate
        dates = [startdate + timedelta(days=i) for i in range(delta.days + 1)]
        return dates
