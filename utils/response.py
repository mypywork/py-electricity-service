import json


class Response:
    def __init__(self, body=None, headers=None, status_code=200):
        self.body = json.dumps(body)
        if headers is None:
            headers = {"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": True}
        self.headers = headers
        self.status_code = status_code

    def to_dict(self):
        response = {
            'headers': self.headers,
            'statusCode': self.status_code,
            'body': self.body
        }
        return response


class AlexaResponse:
    version = "1.0"

    def __init__(self, should_end_session, build_speech, build_card, build_reprompt, build_session_attributes):
        self.should_end_session = should_end_session
        self.build_speech = build_speech
        self.build_card = build_card
        self.build_reprompt = build_reprompt
        self.build_session_attributes = build_session_attributes

    def to_dict(self):
        response = {
            "version": self.version,
            "sessionAttributes": self.build_session_attributes,
            "response": {
                "outputSpeech": self.build_speech,
                "card": self.build_card,
                "reprompt": self.build_reprompt,
                "shouldEndSession": self.should_end_session
            }
        }
        return response

    def to_json(self):
        return json.dumps(self.to_dict())