from handlers.electricity import ElectricityHandler
from handlers.input_text import InputTextHandler
from utils.response import AlexaResponse, Response
from utils.logs import Logger
logger = Logger().get_logger()


@ElectricityHandler
def electricity(event, context, **kwargs):
    logger.debug(f"Event: {event}")
    logger.debug(f"kwargs: {kwargs}")

    response = AlexaResponse(**kwargs).to_dict()
    logger.debug(f"Response: {response}")
    return response

@InputTextHandler
def input_text(event, context, **kwargs):
    logger.debug(f"kwargs: {kwargs}")

    response = Response(**kwargs).to_dict()
    logger.debug(f"Response: {response}")
    return response

# import json
# event = json.load(open("request.json"))
# print(electricity(event, {}))
